package se.leap.bitmaskclient.base.fragments;

import static android.content.Context.MODE_PRIVATE;
import static se.leap.bitmaskclient.R.string.advanced_settings;
import static se.leap.bitmaskclient.base.models.Constants.PREFER_UDP;
import static se.leap.bitmaskclient.base.models.Constants.SHARED_PREFERENCES;
import static se.leap.bitmaskclient.base.models.Constants.USE_BRIDGES;
import static se.leap.bitmaskclient.base.models.Constants.USE_IPv6_FIREWALL;
import static se.leap.bitmaskclient.base.utils.PreferenceHelper.getPreferUDP;
import static se.leap.bitmaskclient.base.utils.PreferenceHelper.getShowAlwaysOnDialog;
import static se.leap.bitmaskclient.base.utils.PreferenceHelper.getUseBridges;
import static se.leap.bitmaskclient.base.utils.PreferenceHelper.getUseSnowflake;
import static se.leap.bitmaskclient.base.utils.PreferenceHelper.hasSnowflakePrefs;
import static se.leap.bitmaskclient.base.utils.PreferenceHelper.preferUDP;
import static se.leap.bitmaskclient.base.utils.PreferenceHelper.useBridges;
import static se.leap.bitmaskclient.base.utils.PreferenceHelper.useSnowflake;
import static se.leap.bitmaskclient.base.utils.ViewHelper.setActionBarTitle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;

import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import de.blinkt.openvpn.core.VpnStatus;
import se.leap.bitmaskclient.BuildConfig;
import se.leap.bitmaskclient.R;
import se.leap.bitmaskclient.base.FragmentManagerEnhanced;
import se.leap.bitmaskclient.base.MainActivity;
import se.leap.bitmaskclient.base.models.Constants;
import se.leap.bitmaskclient.base.models.ProviderObservable;
import se.leap.bitmaskclient.base.utils.LocaleHelper;
import se.leap.bitmaskclient.base.utils.PreferenceHelper;
import se.leap.bitmaskclient.eip.EipCommand;
import se.leap.bitmaskclient.firewall.FirewallManager;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener{

    private FirewallManager firewallManager;
    private SharedPreferences preferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.setPreferredLocale(getContext());
        preferences = requireContext().getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        preferences.registerOnSharedPreferenceChangeListener(this);
        firewallManager = new FirewallManager(requireContext().getApplicationContext(), false);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setActionBarTitle(this, advanced_settings);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, @Nullable String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
        initLanguageEntry();
        initAlwaysOnVpnEntry();
        initExcludeAppsEntry();
        initPreferUDPEntry();
        initUseBridgesEntry();
        initUseSnowflakeEntry();
        initFirewallEntry();
        initTetheringEntry();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        View rootView = getView();
        if (rootView == null)  {
            return;
        }
        if (key.equals(USE_BRIDGES) || key.equals(PREFER_UDP)) {
            initUseBridgesEntry();
            initPreferUDPEntry();
        } else if (key.equals(USE_IPv6_FIREWALL)) {
            initFirewallEntry();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        preferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    private void initLanguageEntry() {
        ListPreference preference = findPreference(requireContext().getString(R.string.pref_language));
        Locale locale = getCurrentLocale();
        assert preference != null;
        preference.setSummary(locale.getDisplayLanguage(locale));
        preference.setOnPreferenceClickListener(preference1 -> {
            setupLanguagePreference((ListPreference) preference1);
            return true;
        });
    }

    private void initAlwaysOnVpnEntry() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Preference preference = findPreference(requireContext().getString(R.string.pref_always_on_vpn));
            assert preference != null;
            preference.setVisible(true);
            preference.setOnPreferenceClickListener(preference1 -> {
                if (getShowAlwaysOnDialog(getContext())) {
                    showAlwaysOnDialog();
                } else {
                    Intent intent = new Intent("android.net.vpn.SETTINGS");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                return true;
            });
        }
    }

    private void initExcludeAppsEntry() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Preference preference = findPreference(requireContext().getString(R.string.pref_exclude_apps));
            assert preference != null;
            preference.setVisible(true);
            Set<String> apps = PreferenceHelper.getExcludedApps(this.getContext());
            if (apps != null) {
                updateExcludeAppsSubtitle(preference, apps.size());
            }
            FragmentManagerEnhanced fragmentManager = new FragmentManagerEnhanced(requireActivity().getSupportFragmentManager());
            preference.setOnPreferenceClickListener(preference1 -> {
                Fragment fragment = new ExcludeAppsFragment();
                fragmentManager.replace(R.id.main_container, fragment, MainActivity.TAG);
                return true;
            });
        }
    }

    private void initPreferUDPEntry() {
        SwitchPreference preference = findPreference(requireContext().getString(R.string.pref_prefer_udp));
        assert preference != null;
        preference.setVisible(true);
        preference.setChecked(getPreferUDP(getContext()));
        preference.setOnPreferenceChangeListener((preference1, newValue) -> {
            preferUDP(getContext(), (Boolean) newValue);
            if (VpnStatus.isVPNActive()) {
                EipCommand.startVPN(getContext(), false);
                Toast.makeText(getContext(), R.string.reconnecting, Toast.LENGTH_LONG).show();
            }
            return true;
        });
        boolean bridgesEnabled = getUseBridges(getContext());
        preference.setEnabled(!bridgesEnabled);
        preference.setSummary(getString(bridgesEnabled ? R.string.disabled_while_bridges_on : R.string.prefer_udp_subtitle));
    }

    private void initUseBridgesEntry() {
        SwitchPreference preference = findPreference(requireContext().getString(R.string.pref_bridges_switch));
        assert preference != null;
        if (ProviderObservable.getInstance().getCurrentProvider().supportsPluggableTransports()) {
            preference.setVisible(true);
            preference.setChecked(getUseBridges(getContext()));
            preference.setOnPreferenceChangeListener((preference1, newValue) -> {
                useBridges(getContext(), (Boolean) newValue);
                if (VpnStatus.isVPNActive()) {
                    EipCommand.startVPN(getContext(), false);
                    Toast.makeText(getContext(), R.string.reconnecting, Toast.LENGTH_LONG).show();
                }
                return true;
            });
            //We check the UI state of the useUdpEntry here as well, in order to avoid a situation
            //where both entries are disabled, because both preferences are enabled.
            //bridges can be enabled not only from here but also from error handling
            boolean useUDP = getPreferUDP(getContext()) && preference.isEnabled();
            preference.setEnabled(!useUDP);
            preference.setSummary(getString(useUDP ? R.string.disabled_while_udp_on : R.string.nav_drawer_subtitle_obfuscated_connection));
        }
        else {
            preference.setEnabled(false);
        }
    }

    private void initUseSnowflakeEntry() {
        SwitchPreference preference = findPreference(requireContext().getString(R.string.pref_snowflake_switch));
        assert preference != null;
        preference.setVisible(true);
        preference.setChecked(hasSnowflakePrefs(getContext()) && getUseSnowflake(getContext()));
        preference.setOnPreferenceChangeListener((preference1, newValue) -> {
            useSnowflake(getContext(), (Boolean) newValue);
            return true;
        });
    }

    private void initFirewallEntry() {
        SwitchPreference preference = findPreference(requireContext().getString(R.string.pref_enableIPv6Firewall));
        assert preference != null;
        preference.setChecked(PreferenceHelper.useIpv6Firewall(getContext()));
        preference.setOnPreferenceChangeListener((preference1, newValue) -> {
            PreferenceHelper.setUseIPv6Firewall(getContext(), (Boolean) newValue);
            if (VpnStatus.isVPNActive()) {
                if ((Boolean) newValue) {
                    firewallManager.startIPv6Firewall();
                } else {
                    firewallManager.stopIPv6Firewall();
                }
            }
            return true;
        });
    }

    private void initTetheringEntry() {
        Preference preference = findPreference(requireContext().getString(R.string.pref_tethering));
        assert preference != null;
        preference.setOnPreferenceClickListener(preference1 -> {
            showTetheringAlert();
            return true;
        });
    }

    public void showAlwaysOnDialog() {
        try {
            FragmentTransaction fragmentTransaction = new FragmentManagerEnhanced(
                    requireActivity().getSupportFragmentManager()).removePreviousFragment(
                    AlwaysOnDialog.TAG);
            DialogFragment newFragment = new AlwaysOnDialog();
            newFragment.show(fragmentTransaction, AlwaysOnDialog.TAG);
        } catch (IllegalStateException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void showTetheringAlert() {
        try {
            FragmentTransaction fragmentTransaction = new FragmentManagerEnhanced(
                    requireActivity().getSupportFragmentManager()).removePreviousFragment(
                    TetheringDialog.TAG);
            DialogFragment newFragment = new TetheringDialog();
            newFragment.show(fragmentTransaction, TetheringDialog.TAG);
        } catch (IllegalStateException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void updateExcludeAppsSubtitle(Preference excludeApps, int number) {
        if (number > 0) {
            excludeApps.setSummary(Html.fromHtml("<font color='red'>" + requireContext().getResources().getQuantityString(R.plurals.subtitle_exclude_apps, number, number) + "</font>"));
        } else {
            excludeApps.setSummary("");
        }
    }

    private Locale getCurrentLocale() {
        String currentLocaleName = Objects.requireNonNull(getPreferenceManager().getSharedPreferences()).getString(Constants.PREFER_LANGUAGE, "");

        Locale currentLocale;
        if (currentLocaleName == null || currentLocaleName.isEmpty()) {
            currentLocale = getResources().getConfiguration().locale;
        }
        else {
            currentLocale = getLocale(currentLocaleName);
        }

        return currentLocale;
    }

    private Locale getLocale(String localeName) {
        Locale locale;

        String[] languageAndRegion = localeName.split("-", 2);
        if (languageAndRegion.length > 1) {
            locale = new Locale(languageAndRegion[0], languageAndRegion[1]);
        } else {
            locale = new Locale(localeName);
        }

        return locale;
    }

    private void setupLanguagePreference(final ListPreference languagePref) {
        Locale[] locales = getLocales();

        final Locale currentLocale = getCurrentLocale();

        String[] displayNames = new String[locales.length];
        String[] entryValues = new String[locales.length];
        for(int index = 0; index < locales.length; index++) {
            Locale locale = locales[index];
            displayNames[index] = locale.getDisplayName(locale);
            entryValues[index] = getLanguageCountryCode(locale);
        }

        languagePref.setValue(getLanguageCountryCode(currentLocale));
        languagePref.setEntries(displayNames);
        languagePref.setEntryValues(entryValues);
        languagePref.setOnPreferenceChangeListener((preference, o) -> {
            languagePref.setValue(o.toString());
            updatePreferredLanguage(o.toString());
            return true;
        });
    }

    private String getLanguageCountryCode(Locale locale) {
        String result = locale.getLanguage();
        if (!locale.getCountry().isEmpty()) {
            result += "-" + locale.getCountry();
        }
        return result;
    }

    private Locale[] getLocales() {
        Locale[] locales = new Locale[BuildConfig.TRANSLATION_ARRAY.length];
        for (int index = 0; index < BuildConfig.TRANSLATION_ARRAY.length; index++) {
            locales[index] = getLocale(BuildConfig.TRANSLATION_ARRAY[index]);
        }
        return locales;
    }

    private void updatePreferredLanguage(String localeName) {
        Objects.requireNonNull(getPreferenceManager().getSharedPreferences()).edit().putString(Constants.PREFER_LANGUAGE, localeName).apply();

        requireActivity().recreate();

        Intent i = requireContext().getPackageManager().getLaunchIntentForPackage( requireContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
