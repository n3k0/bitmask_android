package se.leap.bitmaskclient.base.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import java.util.Locale;

import butterknife.internal.Utils;
import se.leap.bitmaskclient.base.models.Constants;

public class LocaleHelper {
    public static void setPreferredLocale(Context context) {
        String preferredLocale = android.preference.PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.PREFER_LANGUAGE, "");
        if (! preferredLocale.isEmpty()) {
            setLocale(context, preferredLocale);
        }
    }

    private static void setLocale(Context context, String localeName) {
        Locale locale = getLocale(localeName);

        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    public static Locale getLocale(String localeName) {
        Locale locale;
        String[] languageAndRegion = localeName.split("-", 2);
        if (languageAndRegion.length > 1) {
            locale = new Locale(languageAndRegion[0], languageAndRegion[1]);
        } else {
            locale = new Locale(localeName);
        }
        return locale;
    }

}
